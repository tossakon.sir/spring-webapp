FROM adoptopenjdk/openjdk11

EXPOSE 8080

ARG JAR_FILE=target/spring-web-app-reviews-0.0.1-SNAPSHOT.jar

ADD ${JAR_FILE} spring-web-app-reviews.jar

ENTRYPOINT ["java", "-jar", "/spring-web-app-reviews.jar"]

