package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Review;
import com.example.demo.repository.ReviewRepository;

@Service
public class ReviewService {
	@Autowired
	private ReviewRepository reviewRepository;
	
	public List<Review> getAllReviews(){
		return reviewRepository.findAll();
	}
	
	public Review updateReviewById(long id){
		Optional<Review> optional = reviewRepository.findById(id);
		Review review = null;
		if(optional.isPresent()) {
			review = optional.get();
		}else {
			throw new RuntimeException("Not Found id :: "+ id);
		}
		return review;
	}
}
